//
//  GameHelper.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

@testable import TicTacToe

struct GameHelper {
    
    static func makeGame(moveSequence: [Int]) -> Game {
        let game = Game(players: Players(
            player1: .computer(.X),
            player2: .computer(.O)
        ))
        
        for move in moveSequence { game.nextMove(move) }
        return game
    }
    
}
