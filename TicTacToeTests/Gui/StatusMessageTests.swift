//
//  StatusMessageTests.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest
@testable import TicTacToe

class StatusMessageTests: XCTestCase {
    
    func testPlayerXTurn() {
        let game = GameHelper.makeGame(moveSequence: [1, 2])
        XCTAssertEqual(StatusMessage.render(for: game), "Your turn \(Names.X)")
    }
    
    func testPlayerOTurn() {
        let game = GameHelper.makeGame(moveSequence: [1, 2, 3])
        XCTAssertEqual(StatusMessage.render(for: game), "Your turn \(Names.O)")
    }
    
    func testPlayerXWin() {
        let game = GameHelper.makeGame(moveSequence: [1, 4, 2, 5, 3])
        XCTAssertEqual(StatusMessage.render(for: game), "\(Names.X) Won!")
    }
    
    func testPlayerOWin() {
        let game = GameHelper.makeGame(moveSequence: [4, 1, 5, 2, 7, 3])
        XCTAssertEqual(StatusMessage.render(for: game), "\(Names.O) Won!")
    }
    
    func testDraw() {
        let game = GameHelper.makeGame(moveSequence: [1, 2, 3, 5, 8, 4, 6, 9, 7])
        XCTAssertEqual(StatusMessage.render(for: game), "It's a Draw!")
    }
    
}
