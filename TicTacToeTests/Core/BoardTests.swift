//
//  BoardTests.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest
@testable import TicTacToe

class BoardTests: XCTestCase {
    
    func testInitEmpty() {
        let board = Board()
        XCTAssertEqual(board.tiles.count, 9)
    }
    
    func testMakeMove() {
        let board = Board().makeMove(at: 1, by: .X)
        BoardHelper.assertTakenTileAtPosition(board.tiles, at: 1, player: .X)
    }
    
    func testMakeMutlipleMoves() {
        let board = BoardHelper.playMoves([1, 2])
        
        BoardHelper.assertTakenTileAtPosition(board.tiles, at: 1, player: .X)
        BoardHelper.assertTakenTileAtPosition(board.tiles, at: 2, player: .O)
    }
    
    func testImmutableBoard() {
        let b1 = Board()
        let b2 = Board().makeMove(at: 1, by: .X)
        
        BoardHelper.assertEmptyTileAtPosition(b1.tiles, at: 1)
        BoardHelper.assertTakenTileAtPosition(b2.tiles, at: 1, player: .X)
    }
    
    func testValidMove() {
        let board = Board().makeMove(at: 1, by: .X)
        
        XCTAssertTrue(board.validMove(2))
        XCTAssertFalse(board.validMove(1))
    }
    
    func testBoardNonTerminal() {
        let board = BoardHelper.playMoves([1, 2, 3])
        BoardHelper.assertNonTerminal(board.status())
    }
    
    func testPlayerXWin() {
        let board = BoardHelper.playMoves([1, 4, 2, 5, 3])
        BoardHelper.assertPlayerWin(board.status(), .X)
    }
    
    func testPlayerOWin() {
        let board = BoardHelper.playMoves([3, 1, 2, 5, 4, 9])
        BoardHelper.assertPlayerWin(board.status(), .O)
    }
    
    func testDraw() {
        let board = BoardHelper.playMoves([1, 2, 3, 5, 8, 4, 6, 9, 7])
        BoardHelper.assertDraw(board.status())
    }
    
}
