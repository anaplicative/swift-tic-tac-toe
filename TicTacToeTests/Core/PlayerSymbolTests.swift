//
//  PlayerSymbolTests.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest
@testable import TicTacToe

class PlayerSymbolTests: XCTestCase {

    func testOpponent() {
        XCTAssertEqual(.O, PlayerSymbol.X.opponent())
        XCTAssertEqual(.X, PlayerSymbol.O.opponent())
    }

}
