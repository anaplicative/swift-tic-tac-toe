//
//  AITests.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 04/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest
@testable import TicTacToe

class AITests: XCTestCase {
    
    let aiPlayer = AI(asPlayer: .O)
    
    func testBlockDiagonal() {
        assertAiMove(moveSequence: [1, 2, 5], expectedMove: 9)
    }
    
    func testBlockVertical() {
        assertAiMove(moveSequence: [1, 2, 4], expectedMove: 7)
    }
    
    func testBlockHorizontal() {
        assertAiMove(moveSequence: [4, 3, 5], expectedMove: 6)
    }
    
    func testTakeVerticalWin() {
        assertAiMove(moveSequence: [1, 2, 3, 5, 6], expectedMove: 8)
        assertAiMove(moveSequence: [5, 1, 6, 4, 2], expectedMove: 7)
    }
    
    func testTakeCornerWin() {
        assertAiMove(moveSequence: [5, 1, 6, 4, 3], expectedMove: 7)
        assertAiMove(moveSequence: [1, 5, 2, 3, 9], expectedMove: 7)
    }
    
    func testTakeDiagonalWin() {
        assertAiMove(moveSequence: [1, 5, 2, 3, 9], expectedMove: 7)
        assertAiMove(moveSequence: [9, 5, 8, 7, 6], expectedMove: 3)
    }
    
    func testTakeCenter() {
        assertAiMove(moveSequence: [1], expectedMove: 5)
    }
    
    func testTakeCorners() {
        let aiMove = aiPlayer.findMove(Board())
        let corners = [1, 3, 7, 9]
        
        XCTAssertTrue(corners.contains(aiMove))
    }
    
    private func assertAiMove(moveSequence: [Int], expectedMove: Int) {
        let board  = BoardHelper.playMoves(moveSequence)
        let aiMove = aiPlayer.findMove(board)
        
        XCTAssertEqual(aiMove, expectedMove)
    }
    
}
