//
//  PlayersTests.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest
@testable import TicTacToe

class PlayersTests: XCTestCase {
    
    func testInitPlayers() {
        let players = createPlayers()
        XCTAssertEqual(players.currentPlayer.isComputer, false)
    }
    
    func testSwapPlayers() {
        let players = createPlayers()
        
        players.swap()
        
        XCTAssertEqual(players.currentPlayer.isComputer, true)
    }
    
    func testSwapBack() {
        let players = createPlayers()
        
        players.swap()
        players.swap()
        
        XCTAssertEqual(players.currentPlayer.isComputer, false)
    }
    
    private func createPlayers() -> Players {
        return Players(
            player1: .human(.X),
            player2: .computer(.O)
        )
    }
    
}
