//
//  TileTests.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest
@testable import TicTacToe

class TileTests: XCTestCase {
    
    private let tile1 = Tile.Taken(1, .X)
    private let tile2 = Tile.Taken(2, .O)
    private let tile3 = Tile.Empty(3)
    
    func testTileIsTakenBy() {
        XCTAssertTrue(tile1.isTakenBy(.X))
        XCTAssertTrue(tile2.isTakenBy(.O))
    }
    
    func testTileNotTakenBy() {
        XCTAssertFalse(tile1.isTakenBy(.O))
        XCTAssertFalse(tile2.isTakenBy(.X))
    }
    
    func testEmptyTileNotTaken() {
        XCTAssertFalse(tile3.isTakenBy(.X))
        XCTAssertFalse(tile3.isTakenBy(.O))
    }
    
    func testIsEmpty() {
        XCTAssertFalse(tile1.isEmpty())
        XCTAssertFalse(tile2.isEmpty())
        XCTAssertTrue(tile3.isEmpty())
    }
    
    func testPosition() {
        XCTAssertEqual(1, tile1.position())
        XCTAssertEqual(2, tile2.position())
        XCTAssertEqual(3, tile3.position())
    }

}
