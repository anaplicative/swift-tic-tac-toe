//
//  StatusMessage.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

struct StatusMessage {
    
    static func render(for game: Game) -> String {
        switch game.status {
        case .Win(.X):     return "\(Names.X) Won!"
        case .Win(.O):     return "\(Names.O) Won!"
        case .Draw:        return "It's a Draw!"
        case .NonTerminal: return renderTurn(game)
        }
    }
    
    private static func renderTurn(_ game: Game) -> String {
        let player = game.currentPlayer.symbol
        switch player {
        case .X: return "Your turn \(Names.X)"
        case .O: return "Your turn \(Names.O)"
        }
    }
    
}
