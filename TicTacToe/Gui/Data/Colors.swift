//
//  Colors.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

struct Colors {
    static let purple    = UIColor(red: 0.38, green: 0.05, blue: 0.42, alpha: 1.0)
    static let lightPink = UIColor(red: 0.98, green: 0.73, blue: 0.93, alpha: 1.0)
    static let hotPink   = UIColor(red: 0.94, green: 0.08, blue: 0.54, alpha: 1.0)
    static let neonGreen = UIColor(red: 0.48, green: 1.00, blue: 0.48, alpha: 1.0)
    static let neonBlue  = UIColor(red: 0.48, green: 0.94, blue: 1.00, alpha: 1.0)
}
