//
//  AppDelegate.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) -> Bool {
        disableAnimationsForTesting()
        return true
    }
    
    private func disableAnimationsForTesting() {
        if IS_TEST_ENV() {
            UIView.setAnimationsEnabled(false)
        }
    }

}

func IS_TEST_ENV() -> Bool {
    return ProcessInfo().arguments.contains("--uitesting")
}
