//
//  Timer.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

class Timer {
    
    func wait(andThen onFinish: @escaping () -> Void) {
        let delay = getDelay()
        DispatchQueue.main.asyncAfter(
            deadline: .now() + .milliseconds(delay),
            execute: onFinish
        )
    }
    
    private func getDelay() -> Int {
        return IS_TEST_ENV() ? 10 : 1000
    }
    
}
