//
//  RacingFlagTransition.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 07/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

class RacingFlagTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    private let duration: Double = 0.7
    
    private var racingFlag: UIImageView!
    private var toView: UIView!
    private var context: UIViewControllerContextTransitioning!
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        context = transitionContext
        
        if let toView = contextView() {
            self.toView = toView
            racingFlag  = createRacingFlag()
            runAnimation(stepDuration: duration / 2)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    private func runAnimation(stepDuration d: TimeInterval) {
        Animator.runStep(for: d, step: self.coverScreen, onFinish: {
            Animator.runStep(for: d, step: self.leaveScreen, onFinish: self.endTransition)
        })
    }
    
    private func contextView() -> UIView? {
        return context.view(forKey: .to)
    }
    
    private func createRacingFlag() -> UIImageView {
        return RacingFlag(height: height(), width: width()).create()
    }
    
    private func coverScreen() {
        container().addSubview(racingFlag)
        racingFlag.translateY(-height())
    }
    
    private func leaveScreen() {
        container().insertSubview(toView, belowSubview: racingFlag)
        racingFlag.translateY(-height() * 2)
    }
    
    private func endTransition() {
        racingFlag.removeFromSuperview()
        context.completeTransition(true)
    }
    
    private func container() -> UIView {
        return context.containerView
    }
    
    private func height() -> CGFloat {
        return toView.frame.height
    }
    
    private func width() -> CGFloat {
        return toView.frame.width
    }

}

private struct RacingFlag {
    
    var height: CGFloat
    var width: CGFloat
    
    func create() -> UIImageView {
        let frame = CGRect(
            x:      0,
            y:      self.height,
            width:  self.width,
            height: self.height
        )
        let racingFlag = UIImageView(frame: frame)
        racingFlag.contentMode = .scaleAspectFill
        racingFlag.image = UIImage(named: "RacingFlag")
        return racingFlag
    }
    
}
