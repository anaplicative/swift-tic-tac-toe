//
//  ViewController.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

typealias MoveCallback = (Int) -> Void

class BoardViewController: ViewController {
    
    @IBOutlet weak var statusMessage: UILabel!

    var boardView: BoardView!
    var resultView: ResultView!
    var game: Game!
    var timer: Timer!
            
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        evalNextMove()
    }
    
    private func setup() {
        timer = Timer()
        boardView = BoardView(tiles: game.board.tiles, onTap: receiveHumanMove)
        resultView = ResultView(frame: view.frame)
        boardView.center = view.center
        view.backgroundColor = Colors.purple
        view.accessibilityIdentifier = "boardView"
        view.addSubview(boardView)
        view.addSubview(resultView)
    }
    
    private func evalNextMove() {
        let isComputer = game.currentPlayer.isComputer
        
        if game.isOver()   { endGame() }
        else if isComputer { triggerComputerMove() }
        else               { enableButtons() }
    }
    
    private func endGame() {
        disableButtons()
        resultView.showStatus(for: game!.status, onComplete: goToMenu)
    }
    
    private func goToMenu() {
        timer.wait(andThen: navigateBack)
    }
    
    private func navigateBack() {
        navigationController?.popViewController(animated: true)
    }
    
    private func triggerComputerMove() {
        disableButtons()
        timer.wait(andThen: { self.playComputerMove() })
    }
    
    private func playComputerMove() {
        let player = game.currentPlayer.symbol
        let aiMove = AI(asPlayer: player).findMove(game.board)
        playNextMove(aiMove)
        evalNextMove()
    }
        
    private func receiveHumanMove(_ move: Int) {
        playNextMove(move)
        evalNextMove()
    }
    
    private func playNextMove(_ move: Int) {
        game.nextMove(move)
        boardView.refresh(game.board.tiles)
        setStatusMessage()
    }
    
    private func disableButtons() {
        boardView.disableButtons()
    }
    
    private func enableButtons() {
        boardView.enableButtons()
    }
    
    private func setStatusMessage() {
        statusMessage.text = StatusMessage.render(for: game)
    }
    
}
