//
//  MenuViewController.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

class MenuViewController: ViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var humanVsHuman: UIButton!
    @IBOutlet weak var humanVsComputer: UIButton!
    @IBOutlet weak var computerVsComputer: UIButton!
    @IBOutlet weak var ru: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.accessibilityIdentifier = "menuView"
        self.navigationController?.delegate = self
        animateEntrance()
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let id         = segue.identifier,
           let controller = segue.destination as? BoardViewController {
            prepareGameMode(fromSegueId: id, controller)
        }
    }
    
    private func prepareGameMode(fromSegueId id: String, _ controller: BoardViewController) {
        if id == "human-vs-human" {
            controller.game = GameMode.HumanVsHuman.create()
        } else if id == "human-vs-computer" {
            controller.game = GameMode.HumanVsComputer.create()
        } else if id == "computer-vs-computer" {
            controller.game = GameMode.ComputerVsComputer.create()
        }
    }
    
}

// Custom Navigation Animation
extension MenuViewController {
    
    func navigationController(
        _ navigationController: UINavigationController,
        animationControllerFor operation: UINavigationController.Operation,
        from fromVC: UIViewController,
        to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return RacingFlagTransition()
    }

}

// Entrance Animation
private extension MenuViewController {
    
    func animateEntrance() {
        setStartingPositions()
        animateElements()
    }
    
    private func setStartingPositions() {
        let offScreen = -view.frame.width
        let views = [
            mainTitle,
            logo,
            humanVsHuman,
            humanVsComputer,
            computerVsComputer,
            ru
        ]
        for view in views { view?.translateX(offScreen) }
    }
    
    private func animateElements() {
        let viewDelays: [(TimeInterval, UIView)] = [
            (0.0, mainTitle),
            (0.5, ru),
            (0.5, logo),
            (1.2, humanVsHuman),
            (1.3, humanVsComputer),
            (1.4, computerVsComputer)
        ]
        for (delay, view) in viewDelays {
            Animator.springToOrigin(withDelay: delay, duration: 0.8, view)
        }
    }

}
