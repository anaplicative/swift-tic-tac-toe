//
//  ViewController.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 11/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override var prefersStatusBarHidden: Bool {
        return true
    }

}
