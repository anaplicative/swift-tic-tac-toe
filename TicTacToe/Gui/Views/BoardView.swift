//
//  BoardView.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 04/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

class BoardView: UIView {
    
    private let tileButtons : [TileButton]
    private let boardFrame : CGRect = {
        let fullSize = (TileStyle.size * 3) + (TileStyle.spacing * 2)
        return CGRect(
            x: 0,
            y: 0,
            width: fullSize,
            height: fullSize
        )
    }()
    
    init(tiles: Tiles, onTap: @escaping MoveCallback) {
        self.tileButtons = BoardView.createTiles(tiles, onTap)
        super.init(frame: boardFrame)
        appendTiles()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder) not supported")
    }
    
    func enableButtons() {
        tileButtons.forEach({ $0.enableTap() })
    }
    
    func disableButtons() {
        tileButtons.forEach({ $0.disableTap() })
    }
    
    func refresh(_ tiles : Tiles) {
        for (tile, button) in zip(tiles, tileButtons) {
            button.refresh(tile.value)
        }
    }
    
    private static func createTiles(_ tiles: Tiles, _ onTap: @escaping MoveCallback) -> [TileButton] {
        return tiles.values.map({ TileButton(tile: $0, onTap: onTap) })
    }
    
    private func appendTiles() {
        tileButtons.forEach(super.addSubview)
    }
    
}
