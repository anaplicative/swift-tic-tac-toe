//
//  Players.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

struct Player {
    var symbol: PlayerSymbol
    var isComputer: Bool
    
    static func human(_ symbol: PlayerSymbol) -> Player {
        return Player(symbol: symbol, isComputer: false)
    }
    
    static func computer(_ symbol: PlayerSymbol) -> Player {
        return Player(symbol: symbol, isComputer: true)
    }
}

class Players {
    
    var currentPlayer: Player

    private let player1: Player
    private let player2: Player
    
    init(player1: Player, player2: Player) {
        self.player1 = player1
        self.player2 = player2
        currentPlayer = player1
    }
    
    func swap() {
        if currentPlayer1() {
            currentPlayer = player2
        } else {
            currentPlayer = player1
        }
    }
    
    private func currentPlayer1() -> Bool {
        return currentPlayer.symbol     == player1.symbol &&
               currentPlayer.isComputer == player1.isComputer
    }
    
}
