//
//  Game.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

class Game {
    
    private let players: Players
    
    var board: Board
    var currentPlayer: Player { get { return players.currentPlayer } }
    var status: Status { get { return board.status() } }

    init(players: Players) {
        self.players = players
        self.board = Board()
    }
    
    func isOver() -> Bool {
        if case .NonTerminal = status { return false }
        return true
    }
    
    func nextMove(_ move: Int) {
        if board.validMove(move) {
            board = board.makeMove(at: move, by: currentPlayer.symbol)
            players.swap()
        }
    }
    
}
