//
//  AI.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

class AI {
    
    private let minimax: Minimax
    private let center     = 5
    private let corners    = [1, 3, 7, 9]
    private let totalMoves = Board.size * Board.size
    
    init(asPlayer: PlayerSymbol) {
        minimax = Minimax(asPlayer)
    }
    
    func findMove(_ board : Board) -> Int {
        let movesRemaining = board.availableMoves().count
        switch movesRemaining {
        case 9:  return randomCorner()
        case 8:  return secondMove(board)
        default: return minimax.execute(board)
        }
    }
    
    private func secondMove(_ board: Board) -> Int {
        return centerTaken(board) ? randomCorner() : center
    }
    
    private func randomCorner() -> Int {
        return corners.randomElement()!
    }
    
    private func centerTaken(_ board: Board) -> Bool {
        return !board.availableMoves().contains(center)
    }
    
}

private class Minimax {
    
    private let player: PlayerSymbol
    private let opponent: PlayerSymbol
    private let maxDepth: Int = 7
    private let maxValue: Int = 100000
    private let minValue: Int = -100000
    
    init(_ player: PlayerSymbol) {
        self.player   = player
        self.opponent = player.opponent()
    }
    
    func execute(_ board: Board) -> Int {
        return board
            .availableMoves()
            .map({ getMoveScores(board, $0) })
            .reduce(MoveScore(0, minValue), { compareScores($0, $1) })
            .move
    }
    
    private func getMoveScores(_ board: Board, _ move: Int) -> MoveScore {
        let nextBoard = board.makeMove(at: move, by: player)
        return minimize(nextBoard, move, maxDepth, minValue, maxValue)
    }
    
    private func compareScores(_ s1: MoveScore, _ s2: MoveScore) -> MoveScore {
        return s1.score >= s2.score ? s1 : s2
    }
    
    private func minimize(
        _ board: Board,
        _ move: Int,
        _ depth: Int,
        _ alpha : Int,
        _ beta : Int) -> MoveScore {
        if isNonTerminal(board) && depth > 1 {
            var newBeta = beta
            for nextMove in board.availableMoves() {
                let nextBoard = board.makeMove(at: nextMove, by: opponent)
                let nextValue = maximize(nextBoard, nextMove, depth - 1, alpha, newBeta)
                newBeta       = min(newBeta, nextValue.score)
                if alpha >= newBeta {
                    return MoveScore(nextMove, alpha)
                }
            }
            return MoveScore(move, newBeta)
        }
        return heuristicValue(board, move, depth)
    }
    
    private func maximize(
        _ board: Board,
        _ move: Int,
        _ depth: Int,
        _ alpha: Int,
        _ beta: Int) -> MoveScore {
        if isNonTerminal(board) && depth > 1 {
            var newAlpha = alpha
            for nextMove in board.availableMoves() {
                let nextBoard = board.makeMove(at: nextMove, by: player)
                let nextValue = minimize(nextBoard, nextMove, depth - 1, newAlpha, beta)
                newAlpha      = max(newAlpha, nextValue.score)
                if newAlpha >= beta {
                    return MoveScore(nextMove, beta)
                }
            }
            return MoveScore(move, newAlpha)
        }
        return heuristicValue(board, move, depth)
    }
    
    private func isNonTerminal(_ board: Board) -> Bool {
        switch board.status() {
        case .NonTerminal: return true
        default:           return false
        }
    }
    
    private func heuristicValue(_ board: Board, _ move: Int, _ depth: Int) -> MoveScore  {
        switch board.status() {
        case let .Win(p) where p == player:   return MoveScore(move, depth)
        case let .Win(p) where p == opponent: return MoveScore(move, -depth)
        default:                              return MoveScore(move, 0)
        }
    }
    
}

private struct MoveScore {
    var move: Int
    var score: Int
    
    init(_ move: Int, _ score: Int) {
        self.move = move
        self.score = score
    }
}
