//
//  Tile.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

enum Tile: CustomStringConvertible {

    case Taken(Int, PlayerSymbol)
    case Empty(Int)
    
    func position() -> Int {
        switch self {
        case let .Taken(i, _): return i
        case let .Empty(i):    return i
        }
    }
    
    func isTakenBy(_ player: PlayerSymbol) -> Bool {
        switch self {
        case let .Taken(_, p): return p == player
        default:               return false
        }
    }
    
    func isEmpty() -> Bool {
        switch self {
        case .Empty(_): return true
        default:        return false
        }
    }
    
    var description: String {
        switch self {
        case let .Taken(i, p): return "Tile-\(i)-\(p)"
        case let .Empty(i):    return "Tile-\(i)"
        }
    }

}
