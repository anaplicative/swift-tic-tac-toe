//
//  PlayerSymbol.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

enum PlayerSymbol: String {
    case X, O
    
    func opponent() -> PlayerSymbol {
        switch self {
        case .X: return .O
        case .O: return .X
        }
    }
}
