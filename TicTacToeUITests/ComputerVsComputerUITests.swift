//
//  ComputerVsComputerUITests.swift
//  TicTacToeUITests
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest

class ComputerVsComputerUITests: GameUITestCase {
    
    override func setUp() {
        super.setUp()
        app.launch()
    }
    
    func testBoard() {
        // setup
        computerVComputerGame()
        
        // assertions
        assertAllTilesTaken()
    }
    
    private func computerVComputerGame() {
        app.buttons["Computer Vs Computer"].tap()
    }
    
    private func assertAllTilesTaken() {
        let btns = (1...9).map({ app.buttons["Tile-\($0)"] })
        waitForButtons(btns)
    }
    
    private func assertResultDrawImage() {
        let draw = shouldExist(app.otherElements["draw"])
        wait(for: [draw], timeout: 2)
    }
    
    func waitForButtons(_ buttons: [XCUIElement]) {
        let allButtons = buttons.map({ self.shouldNotExist($0) })
        wait(for: allButtons, timeout: 2)
    }
    
}
