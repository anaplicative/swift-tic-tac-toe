//
//  GameUITestCase.swift
//  TicTacToeUITests
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest

class GameUITestCase: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launchArguments.append("--uitesting")
    }
    
    func shouldExist(_ element: XCUIElement) -> XCTestExpectation {
        return expect(element, toExist)
    }
    
    func shouldNotExist(_ element: XCUIElement) -> XCTestExpectation {
        return expect(element, toNotExist)
    }
    
    private func expect(_ element: XCUIElement, _ predicate: NSPredicate) -> XCTestExpectation {
        return expectation(
            for: predicate,
            evaluatedWith: element,
            handler: nil
        )
    }
    
    private let toExist       = NSPredicate(format: "exists = true")
    private let toNotExist = NSPredicate(format: "exists = false")
    
}
